﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Logs.Loggers
{
    internal class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine($"[{DateTime.Now}] {message}");
        }

        public void Log(ErrorMessageCode code, Exception exception)
        {
            Console.WriteLine($"[{DateTime.Now}]: {code}\n{exception}");
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Logs
{
    internal interface ILogger
    {
        void Log(string message);
        void Log(ErrorMessageCode code, Exception exception);
    }
}

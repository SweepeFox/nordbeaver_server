﻿using System;
using System.Diagnostics;

namespace By.Gamefactory.NordBeaver
{
    class Program
    {
        const string CONFIG_ARG_KEY = "-c";
        const string EXIT_COMMAND = "exit";

        private static string pathToServerConfig = "";

        static void Main(string[] args)
        {
            ParseCliArguments(args);
            Server server = new Server(pathToServerConfig);
            server.Start();
            bool stopServer = false;
            while (!stopServer)
            {
                string cmd = Console.ReadLine();
                if (cmd.Equals(EXIT_COMMAND))
                {
                    server.Stop();
                    stopServer = true;
                }
            }
        }

        static void ParseCliArguments(string[] args)
        {
            if (args.Length == 0)
            {
                return;
            }
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case CONFIG_ARG_KEY:
                        if (i + 1 < args.Length)
                        {
                            pathToServerConfig = args[i + 1];
                        }
                        else
                        {
                            Console.WriteLine("You must provide path to config file with -c option.");
                            Process.GetCurrentProcess().Kill();
                        }
                        break;
                }
            }
        }
    }
}

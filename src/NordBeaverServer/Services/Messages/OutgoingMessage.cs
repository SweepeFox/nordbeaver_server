﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Messages
{
    internal class OutgoingMessage
    {
        public int[] ToPeers;
        public int Peer;
        public bool Single;
        public int RoomId;
        public DefaultMessage Message;

        public OutgoingMessage()
        { }

        private OutgoingMessage(int roomId, DefaultMessage message)
        {
            RoomId = roomId;
            Message = message;
        }

        public OutgoingMessage(int[] peers, int roomId, DefaultMessage message)
            : this(roomId, message)
        {
            ToPeers = peers;
            Single = false;
        }

        public OutgoingMessage(int peer, int roomId, DefaultMessage message)
            : this(roomId, message)
        {
            Peer = peer;
            Single = true;
        }
    }
}

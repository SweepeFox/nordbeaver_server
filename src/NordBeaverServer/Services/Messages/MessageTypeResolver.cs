﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;

namespace By.Gamefactory.NordBeaver.Services.Messages
{
    public static class MessageTypeResolver
    {
        private static IReadOnlyDictionary<byte, Type> TypeCodeToTypeMap = new Dictionary<byte, Type>
        {
            { 0, typeof(DefaultMessage) },
            { 1, typeof(JoinRoomMessage) },
            { 2, typeof(GameStateMessage) },
            { 3, typeof(GameConfigMessage) },
            { 4, typeof(ErrorMessage) },
            { 5, typeof(UserInputMessage) },
            { 6, typeof(DisconnectMessage) },
            { 7, typeof(RoomStateMessage) },
            { 8, typeof(ReadyToPlayMessage) },
            { 9, typeof(PingMessage) },
            { 10, typeof(PongMessage) }
        };

        private static IReadOnlyDictionary<Type, byte> TypeToTypeCodeMap = new Dictionary<Type, byte>
        {
            { typeof(DefaultMessage), 0 },
            { typeof(JoinRoomMessage), 1 },
            { typeof(GameStateMessage), 2 },
            { typeof(GameConfigMessage), 3 },
            { typeof(ErrorMessage), 4 },
            { typeof(UserInputMessage), 5 },
            { typeof(DisconnectMessage), 6 },
            { typeof(RoomStateMessage), 7 },
            { typeof(ReadyToPlayMessage), 8 },
            { typeof(PingMessage), 9 },
            { typeof(PongMessage), 10 }
        };

        public static byte TypeToTypeCode(Type type)
        {
            return TypeToTypeCodeMap[type];
        }

        public static Type TypeCodeToType(byte typeCode)
        {
            return TypeCodeToTypeMap[typeCode];
        }
    }
}

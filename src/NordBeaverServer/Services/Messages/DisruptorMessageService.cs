﻿using By.Gamefactory.NordBeaver.Network;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using Disruptor;
using Disruptor.Dsl;

namespace By.Gamefactory.NordBeaver.Services.Messages
{
    class DisruptorMessageService
    {
        private readonly Disruptor<IncomingMessage> inMessages;
        private readonly Disruptor<OutgoingMessage> outMessages;
        private readonly Disruptor<ForceDisconnect> forceDisconnectPeers;
        private bool started;

        public DisruptorMessageService(MessagesServiceConfig messagesServiceConfig)
        {
            inMessages = new Disruptor<IncomingMessage>(() => new IncomingMessage(), messagesServiceConfig.IncomingMessageQueueLength);
            outMessages = new Disruptor<OutgoingMessage>(() => new OutgoingMessage(), messagesServiceConfig.OutgoingMessageQueueLength);
            forceDisconnectPeers = new Disruptor<ForceDisconnect>(() => new ForceDisconnect(), messagesServiceConfig.OutgoingMessageQueueLength);
        }
        
        public void Start(
            IEventHandler<OutgoingMessage> outMessageHandler, 
            IEventHandler<IncomingMessage> inMessageHandler,
            IEventHandler<ForceDisconnect> forceDisconnectHandler)
        {
            inMessages.HandleEventsWith(inMessageHandler);
            outMessages.HandleEventsWith(outMessageHandler);
            forceDisconnectPeers.HandleEventsWith(forceDisconnectHandler);
            inMessages.Start();
            outMessages.Start();
            forceDisconnectPeers.Start();
            started = true;
        }

        public void EnqueueIncomingMessage(int peer, int roomId, DefaultMessage message)
        {
            if (!started)
            {
                return;
            }
            using (var scope = inMessages.PublishEvent())
            {
                IncomingMessage inMessage = scope.Event();
                inMessage.fromPeer = peer;
                inMessage.roomId = roomId;
                inMessage.message = message;
            }
        }

        public void EnqueueOutgoingMessage(int[] peers, int roomId, DefaultMessage message)
        {
            if (!started)
            {
                return;
            }
            using (var scope = outMessages.PublishEvent())
            {
                OutgoingMessage outMessage = scope.Event();
                outMessage.Single = false;
                outMessage.ToPeers = peers;
                outMessage.RoomId = roomId;
                outMessage.Message = message;
            }
        }

        public void EnqueueOutgoingMessage(int peer, int roomId, DefaultMessage message)
        {
            if (!started)
            {
                return;
            }
            using (var scope = outMessages.PublishEvent())
            {
                OutgoingMessage outMessage = scope.Event();
                outMessage.Single = true;
                outMessage.Peer = peer;
                outMessage.RoomId = roomId;
                outMessage.Message = message;
            }
        }

        public void EnqueueForceDisconnect(int peer)
        {
            if (!started)
            {
                return;
            }
            using (var scope = forceDisconnectPeers.PublishEvent())
            {
                ForceDisconnect message = scope.Event();
                message.peer = peer;
            }
        }

        public void Stop()
        {
            started = false;
            inMessages.Shutdown();
            outMessages.Shutdown();
        }
    }
}

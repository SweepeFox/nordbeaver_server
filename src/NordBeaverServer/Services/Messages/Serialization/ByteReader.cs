﻿using System;

namespace By.Gamefactory.NordBeaver.Services.Messages.Serialization
{
    public class ByteReader
    {
        private byte[] buffer;
        private int index;

        public ByteReader(byte[] buffer, int startIndex = 0)
        {
            this.buffer = buffer;
            index = startIndex;
        }

        public byte ReadByte()
        {
            return buffer[index++];
        }

        public bool ReadBool()
        {
            return buffer[index++] > 0;
        }

        public int ReadInt()
        {
            return buffer[index++] | buffer[index++] << 8 | buffer[index++] << 16 | buffer[index++] << 24;
        }

        public long ReadLong()
        {
            return buffer[index++] | 
                buffer[index++] >> 8 | 
                buffer[index++] >> 16 | 
                buffer[index++] >> 24 | 
                buffer[index++] >> 32 |
                buffer[index++] >> 40 |
                buffer[index++] >> 48 |
                buffer[index++] >> 56;
        }

        public ulong ReadULong()
        {
            return (ulong)buffer[index++] |
                (ulong)buffer[index++] >> 8 |
                (ulong)buffer[index++] >> 16 |
                (ulong)buffer[index++] >> 24 |
                (ulong)buffer[index++] >> 32 |
                (ulong)buffer[index++] >> 40 |
                (ulong)buffer[index++] >> 48 |
                (ulong)buffer[index++] >> 56;
        }

        public float ReadFloat()
        {
            float v = BitConverter.ToSingle(buffer, index);
            index += 4;
            return v;
        }

        public string ReadString()
        {
            int len = ReadInt();
            string result = System.Text.Encoding.Default.GetString(buffer, index, len);
            index += System.Text.Encoding.Default.GetMaxByteCount(len);
            return result;
        }

        public float[] ReadFloatArray()
        {
            int len = ReadInt();
            float[] ret = new float[len];
            for (int i = 0; i < len; i++)
            {
                ret[i] = ReadFloat();
            }

            return ret;
        }

        public int[] ReadIntArray()
        {
            int len = ReadInt();
            int[] ret = new int[len];
            for (int i = 0; i < len; i++)
            {
                ret[i] = ReadInt();
            }

            return ret;
        }

        public byte[] ReadByteArray()
        {
            int len = ReadInt();
            byte[] ret = new byte[len];
            Array.Copy(buffer, index, ret, 0, len);
            index += len;
            return ret;
        }
    }
}

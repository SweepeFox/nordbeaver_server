﻿using System;

namespace By.Gamefactory.NordBeaver.Services.Messages.Serialization
{
    public class ByteWriter
    {
        private byte[] buffer;
        private int index;

        public ByteWriter(byte[] buffer, int startIndex = 0)
        {
            this.buffer = buffer;
            index = startIndex;
        }

        public ByteWriter(int size)
        {
            buffer = new byte[size];
            index = 0;
        }

        public void Write(byte v)
        {
            buffer[index++] = v;
        }

        public void Write(bool v)
        {
            if (v) buffer[index++] = 0x01;
            else buffer[index++] = 0x00;
        }

        public void Write(int v)
        {
            buffer[index++] = (byte)v;
            buffer[index++] = (byte)(v >> 8);
            buffer[index++] = (byte)(v >> 16);
            buffer[index++] = (byte)(v >> 24);
        }

        public void Write(long v)
        {
            buffer[index++] = (byte)v;
            buffer[index++] = (byte)(v >> 8);
            buffer[index++] = (byte)(v >> 16);
            buffer[index++] = (byte)(v >> 24);
            buffer[index++] = (byte)(v >> 32);
            buffer[index++] = (byte)(v >> 40);
            buffer[index++] = (byte)(v >> 48);
            buffer[index++] = (byte)(v >> 56);
        }

        public void Write(ulong v)
        {
            buffer[index++] = (byte)v;
            buffer[index++] = (byte)(v >> 8);
            buffer[index++] = (byte)(v >> 16);
            buffer[index++] = (byte)(v >> 24);
            buffer[index++] = (byte)(v >> 32);
            buffer[index++] = (byte)(v >> 40);
            buffer[index++] = (byte)(v >> 48);
            buffer[index++] = (byte)(v >> 56);
        }

        public void Write(float v)
        {
            Buffer.BlockCopy(BitConverter.GetBytes(v), 0, buffer, index, 4);
            index += 4;
        }

        public void Write(string v)
        {
            byte[] strBytes = System.Text.Encoding.Default.GetBytes(v);
            int len = strBytes.Length;
            Write(len);
            for (int j = 0; j < len; j++)
            {
                buffer[index++] = strBytes[j];
            }
        }

        public void Write(float[] v)
        {
            Write(v.Length);
            for (int i = 0; i < v.Length; i++)
            {
                Write(v[i]);
            }
        }

        public void Write(int[] v)
        {
            Write(v.Length);
            for (int i = 0; i < v.Length; i++)
            {
                Write(v[i]);
            }
        }

        public void Write(byte[] v)
        {
            int vLength = v.Length;
            Write(vLength);
            Array.Copy(v, 0, buffer, index, vLength);
            index += vLength;
        }

        public byte[] GetBytes()
        {
            return buffer;
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class UserInputMessage : DefaultMessage
    {
        public int position;
        public int screenX;
        public int screenY;
        public byte direction;
        public byte animationState;
        public bool plantedBomb;

        public override void FromBytes(byte[] bytes)
        {
            ByteReader byteReader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            position = byteReader.ReadInt();
            screenX = byteReader.ReadInt();
            screenY = byteReader.ReadInt();
            direction = byteReader.ReadByte();
            animationState = byteReader.ReadByte();
            plantedBomb = byteReader.ReadBool();
        }

        protected override int GetMessageSize()
        {
            return sizeof(int) * 3 + sizeof(byte) * 2 + sizeof(bool);
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(position);
            byteWriter.Write(screenX);
            byteWriter.Write(screenY);
            byteWriter.Write(direction);
            byteWriter.Write(animationState);
            byteWriter.Write(plantedBomb);
        }
    }
}

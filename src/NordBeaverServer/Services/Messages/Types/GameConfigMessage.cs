﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class GameConfigMessage : DefaultMessage
    {
        public byte[] allTimeRecords;
        public byte[] monthRecords;
        public byte[] weekRecords;

        override protected int GetMessageSize()
        {
            return sizeof(int) * 3 + allTimeRecords.Length + monthRecords.Length + weekRecords.Length;
        }

        override protected void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(allTimeRecords);
            byteWriter.Write(monthRecords);
            byteWriter.Write(weekRecords);
        }

        override public void FromBytes(byte[] bytes)
        {
            ByteReader reader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            allTimeRecords = reader.ReadByteArray();
            monthRecords = reader.ReadByteArray();
            weekRecords = reader.ReadByteArray();
        }
    }
}

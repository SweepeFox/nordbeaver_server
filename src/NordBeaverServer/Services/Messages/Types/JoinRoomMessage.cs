﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class JoinRoomMessage : DefaultMessage
    {
        public int userId;
        public int battleId;

        public override void FromBytes(byte[] bytes)
        {
            ByteReader reader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            userId = reader.ReadInt();
            battleId = reader.ReadInt();
        }

        protected override int GetMessageSize()
        {
            return sizeof(int) * 2;
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(userId);
            byteWriter.Write(battleId);
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class PongMessage : DefaultMessage
    {
        public override void FromBytes(byte[] bytes)
        {
        }

        protected override int GetMessageSize()
        {
            return 0;
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
        }
    }
}

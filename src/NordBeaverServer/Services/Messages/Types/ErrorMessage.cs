﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class ErrorMessage : DefaultMessage
    {
        public byte errorCode;

        public override void FromBytes(byte[] bytes)
        {
            ByteReader reader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            errorCode = reader.ReadByte();
        }

        protected override int GetMessageSize()
        {
            return sizeof(byte);
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(errorCode);
        }
    }

    enum ErrorMessageCode
    {
        GameAlreadyStarted,
        RoomIsFull,
        FailedToRejoinTheGame,
        FailedToSendMessageToRoom,
        FailedToAddNewRoom,
        FailToProcessDisconnectMessage,
        FailedToProcessFinishedRooms,
        RoomFinished,
        FailedToJoinRoom,
        FailedToStartGame,
        FailedToSendGroupMessage,
        GameFinished,
        FailedToSetReady,
        ReadyNotSet,
        NotEnoughMindplaysUsersToStartTheGame
    }
}

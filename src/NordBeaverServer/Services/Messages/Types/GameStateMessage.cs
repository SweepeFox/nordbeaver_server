﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class GameStateMessage : DefaultMessage
    {
        public byte[] player;
        public byte gameState;
        public int stateTime;

        public override void FromBytes(byte[] bytes)
        {
            ByteReader reader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            player = reader.ReadByteArray();
            gameState = reader.ReadByte();
            stateTime = reader.ReadInt();
        }

        protected override int GetMessageSize()
        {
            return sizeof(int) * 2 + sizeof(byte) + player.Length;
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(player);
            byteWriter.Write(gameState);
            byteWriter.Write(stateTime);
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    internal class RoomStateMessage : DefaultMessage
    {
        public byte state;
        public byte stateTimerSec;

        public override void FromBytes(byte[] bytes)
        {
            ByteReader reader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            state = reader.ReadByte();
            stateTimerSec = reader.ReadByte();
        }

        protected override int GetMessageSize()
        {
            return 2 * sizeof(byte);
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write(state);
            byteWriter.Write(stateTimerSec);
        }
    }
}

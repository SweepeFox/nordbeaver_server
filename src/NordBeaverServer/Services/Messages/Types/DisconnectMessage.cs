﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    class DisconnectMessage : DefaultMessage
    {
        public DisconnectReason reason;
        public override void FromBytes(byte[] bytes)
        {
            ByteReader byteReader = new ByteReader(bytes, MESSAGE_HEADER_SIZE);
            reason = (DisconnectReason)byteReader.ReadByte();
        }

        protected override int GetMessageSize()
        {
            return sizeof(byte);
        }

        protected override void WriteMessageBytes(ByteWriter byteWriter)
        {
            byteWriter.Write((byte)reason);
        }
    }

    enum DisconnectReason
    {
        SocketClosed,
        UserRequestsDisconnect,
        RoomFinished
    }
}

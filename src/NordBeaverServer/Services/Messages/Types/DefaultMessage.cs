﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;

namespace By.Gamefactory.NordBeaver.Services.Messages.Types
{
    abstract class DefaultMessage
    {
        public const int MESSAGE_HEADER_SIZE = sizeof(int)  //Message length
                                             + sizeof(byte) //Message type
                                             + sizeof(int); //Room Id
        public const int MESSAGE_LENGTH_POSITION = 0;
        public const int MESSAGE_TYPE_POSITION = sizeof(int);
        public const int MESSAGE_ROOMID_POSITION = sizeof(int) + sizeof(byte);

        public DefaultMessage()
        {
        }

        protected abstract int GetMessageSize();
        protected abstract void WriteMessageBytes(ByteWriter byteWriter);

        public byte[] ToBytes(int roomId)
        {
            int dataLength = GetMessageSize();
            ByteWriter writer = new ByteWriter(MESSAGE_HEADER_SIZE + dataLength);
            writer.Write(dataLength);
            writer.Write(MessageTypeResolver.TypeToTypeCode(GetType()));
            writer.Write(roomId);
            WriteMessageBytes(writer);
            return writer.GetBytes();
        }

        public abstract void FromBytes(byte[] bytes);
    }
}

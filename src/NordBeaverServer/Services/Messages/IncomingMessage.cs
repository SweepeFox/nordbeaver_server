﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Messages
{
    internal class IncomingMessage
    {
        public int fromPeer;
        public int roomId;
        public DefaultMessage message;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Messages
{
    class MessagesServiceConfig
    {
        public readonly int IncomingMessageQueueLength = 2048;
        public readonly int OutgoingMessageQueueLength = 2048;
        public readonly int IncomingQueueThreadWaitForMessages = 1000;
        public readonly int OutgoingQueueThreadWaitForMessages = 1000;
    }
}

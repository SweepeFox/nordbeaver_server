﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace By.Gamefactory.NordBeaver.Services.EventLoops
{
    class FramePulser
    {
        public static readonly int FPS = 64;
        public static readonly int FRAME_TIME_MS = 1000 / FPS;
        public event Action<long> OnPulse;
        private bool stopPulsate;

        private Thread pulsingThread;

        public FramePulser()
        {
            stopPulsate = false;
        }

        public void Start()
        {
            pulsingThread = new Thread(new ThreadStart(() =>
            {
                DateTime startTime = DateTime.UtcNow;
                long millisecondsPassed = 0;
                while (!stopPulsate)
                {
                    if (millisecondsPassed >= FRAME_TIME_MS)
                    {
                        OnPulse?.Invoke(millisecondsPassed);
                        startTime = startTime.AddMilliseconds(millisecondsPassed);
                    }
                    millisecondsPassed = DateTime.UtcNow.Subtract(startTime).Milliseconds;
                }
            }));
            pulsingThread.Start();
        }

        public void Stop()
        {
            stopPulsate = true;
        }
    }
}

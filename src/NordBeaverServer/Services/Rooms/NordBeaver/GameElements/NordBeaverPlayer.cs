﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;
using By.Gamefactory.NordBeaver.Services.Messages.Types;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameElements
{
    class NordBeaverPlayer
    {
        public static readonly int SERIALIZATION_SIZE = sizeof(int) +       // UserId
                                                        sizeof(int) +       // Position
                                                        sizeof(int) +       // Score
                                                        sizeof(int);        //Nickname

        public static readonly byte UPDATE_MSG_PER_SECOND = 10;

        public readonly int UserId;
        public int Position { get; private set; }
        public int Score { get; private set; }
        public string Nickname { get; private set; }

        private byte[] playerData;

        public NordBeaverPlayer(int userId)
        {
            UserId = userId;
            Position = 0;
            Score = 0;
            Nickname = "Denis";
            playerData = new byte[SERIALIZATION_SIZE + Nickname.Length];
        }

        public void UpdatePlayer(UserInputMessage message)
        {
            
        }

        public void AddScore(sbyte amount)
        {
            Score += amount;
        }

        public byte[] ToBytes()
        {
            ByteWriter byteWriter = new ByteWriter(playerData);
            byteWriter.Write(UserId);
            byteWriter.Write(Position);
            byteWriter.Write(Score);
            byteWriter.Write(Nickname);
            return playerData;
        }
    }
}

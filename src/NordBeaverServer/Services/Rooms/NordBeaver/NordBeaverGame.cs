﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.EventLoops;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameElements;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver
{
    class NordBeaverGame
    {
        private readonly FramePulser framePulser;
        private Gamefield gamefield;
        private NordBeaverPlayer player;
        private readonly ILogger logger;
        private IGameState currentState;
        private IGameState finished;
        public bool Finished { get => currentState == finished; }

        public NordBeaverGame(FramePulser framePulser, ILogger logger)
        {
            this.framePulser = framePulser;
            this.logger = logger;
        }

        private void FramePulser_OnPulse(long dt)
        {
            IGameState nextState = currentState.Process(dt);
            if (nextState != currentState)
            {
                currentState.OnExit();
                currentState = nextState;
                currentState.OnEnter();
            }
        }

        public void Start(RoomUser roomUser)
        {
            player = new NordBeaverPlayer(roomUser.userId);
            gamefield = new Gamefield();
            finished = new FinishedGameState(logger);
            IGameState started = new StartedGameState(finished, player, logger);
            IGameState countdown = new CountdownGameState(started, logger);
            IGameState created = new CreatedGameState(countdown, logger);
            currentState = created;
            currentState.OnEnter();
            framePulser.OnPulse += FramePulser_OnPulse;
            currentState.OnExit();
            currentState = currentState.ForceSwitch();
            currentState.OnEnter();
        }

        public void ProcessMessage(int peer, DefaultMessage message)
        {
            currentState?.OnMessage(peer, message);
        }

        public GameStateMessage GetGameState()
        {
            GameStateMessage message = new GameStateMessage()
            {
                player = player.ToBytes(),
                gameState = currentState.Id,
                stateTime = (int)(currentState.GetTime() / 1000)
            };
            return message;
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace NordBeaver.Services.Rooms.NordBeaver
{
    class TestRecords
    {
        private List<Record> allTimeRecords = new List<Record>();
        private List<Record> monthRecords = new List<Record>();
        private List<Record> weekRecords = new List<Record>();

        public TestRecords()
        {
            for (int i = 0; i < 5; i++)
            {
                var allTimeRecord = new Record();
                allTimeRecord.nickname = $"ATUser {i}";
                allTimeRecord.score = i;
                allTimeRecords.Add(allTimeRecord);

                var monthRecord = new Record();
                monthRecord.nickname = $"MUser {i}";
                monthRecord.score = i;
                monthRecords.Add(monthRecord);

                var weekRecord = new Record();
                weekRecord.nickname = $"WUser {i}";
                weekRecord.score = i;
                weekRecords.Add(weekRecord);
            }
        }

        public byte[] AllTimeRecordsToBytes()
        {
            var serializationOffset = 0;
            var serializedRecordsBuffer = new byte[allTimeRecords.Count * Record.SERIALIZATION_SIZE];
            foreach (Record record in allTimeRecords)
            {
                byte[] serializedRecord = record.ToBytes();
                Array.Copy(serializedRecord, 0, serializedRecordsBuffer, serializationOffset, serializedRecord.Length);
                serializationOffset += Record.SERIALIZATION_SIZE;
            }
            return serializedRecordsBuffer;
        }

        public byte[] MonthRecordsToBytes()
        {
            var serializationOffset = 0;
            var serializedRecordsBuffer = new byte[monthRecords.Count * Record.SERIALIZATION_SIZE];
            foreach (Record record in monthRecords)
            {
                byte[] serializedRecord = record.ToBytes();
                Array.Copy(serializedRecord, 0, serializedRecordsBuffer, serializationOffset, serializedRecord.Length);
                serializationOffset += Record.SERIALIZATION_SIZE;
            }
            return serializedRecordsBuffer;
        }
        public byte[] WeekRecordsToBytes()
        {
            var serializationOffset = 0;
            var serializedRecordsBuffer = new byte[weekRecords.Count * Record.SERIALIZATION_SIZE];
            foreach (Record record in weekRecords)
            {
                byte[] serializedRecord = record.ToBytes();
                Array.Copy(serializedRecord, 0, serializedRecordsBuffer, serializationOffset, serializedRecord.Length);
                serializationOffset += Record.SERIALIZATION_SIZE;
            }
            return serializedRecordsBuffer;
        }

    }
}

class Record
{
    public static readonly int SERIALIZATION_SIZE = sizeof(int) +           // Nickname
                                                        sizeof(int);       // Score
    public string nickname;
    public int score;

    private byte[] data;

    public byte[] ToBytes()
    {
        ByteWriter byteWriter = new ByteWriter(data);
        byteWriter.Write(nickname);
        byteWriter.Write(score);
        return data;
    }
}

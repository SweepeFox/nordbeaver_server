﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal class FinishedGameState : IGameState
    {
        private readonly ILogger logger;

        public FinishedGameState(ILogger logger)
        {
            this.logger = logger;
        }

        public byte Id => (byte)GameStateId.Finished;

        public IGameState ForceSwitch()
        {
            return this;
        }

        public long GetTime()
        {
            return 0;
        }

        public void OnEnter()
        {
            logger.Log("FinishedGameState.OnEnter");

        }

        public void OnExit()
        {
        }

        public void OnMessage(int peer, DefaultMessage message)
        {
        }

        public IGameState Process(long dt)
        {
            return this;
        }
    }
}

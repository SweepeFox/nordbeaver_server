﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal class CountdownGameState : IGameState
    {
        private readonly IGameState next;
        private readonly ILogger logger;

        public CountdownGameState(IGameState next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public byte Id => (byte)GameStateId.CountdownToStart;

        public IGameState ForceSwitch()
        {
            return next;
        }

        public long GetTime()
        {
            return 0;
        }

        public void OnEnter()
        {
            logger.Log("CountdownGameState.OnEnter");
        }

        public void OnExit()
        {
        }

        public void OnMessage(int peer, DefaultMessage message)
        {
        }

        public IGameState Process(long dt)
        {
            return next;
        }
    }
}

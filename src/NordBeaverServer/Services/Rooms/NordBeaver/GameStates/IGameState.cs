﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal interface IGameState
    {
        byte Id { get; }
        void OnEnter();
        void OnExit();
        IGameState Process(long dt);
        IGameState ForceSwitch();
        void OnMessage(int peer, DefaultMessage message);
        long GetTime();
    }
}

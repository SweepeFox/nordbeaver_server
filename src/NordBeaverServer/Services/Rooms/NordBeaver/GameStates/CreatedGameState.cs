﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal class CreatedGameState : IGameState
    {
        private readonly IGameState next;
        private readonly ILogger logger;

        public CreatedGameState(IGameState next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public byte Id => (byte)GameStateId.Created;

        public IGameState ForceSwitch()
        {
            return next;
        }

        public long GetTime()
        {
            return 0;
        }

        public void OnEnter()
        {
            logger.Log("CreatedGameState.OnEnter");
        }

        public void OnExit()
        {
            logger.Log("CreatedGameState.OnExit");
        }

        public void OnMessage(int peer, DefaultMessage message)
        {
        }

        public IGameState Process(long dt)
        {
            return this;
        }
    }
}

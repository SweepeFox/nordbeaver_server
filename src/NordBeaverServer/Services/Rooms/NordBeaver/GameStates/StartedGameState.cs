﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameElements;
using System;
using System.Collections.Generic;
using System.Threading;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal class StartedGameState : IGameState
    {
        private readonly IGameState next;
        private readonly NordBeaverPlayer player;
        private readonly ILogger logger;

        public StartedGameState(IGameState next, NordBeaverPlayer player, ILogger logger)
        {
            this.next = next;
            this.player = player;
            this.logger = logger;
        }

        public byte Id => (byte)GameStateId.Started;

        public IGameState ForceSwitch()
        {
            return next;
        }

        public long GetTime()
        {
            return 0;
        }

        public void OnEnter()
        {
            logger.Log("StartedGameState.OnEnter");
        }

        public void OnExit()
        {
            CalculateRewards();
        }

        public void OnMessage(int peer, DefaultMessage message)
        {
            switch (message)
            {
                case UserInputMessage userInputMessage:
                    player.UpdatePlayer(userInputMessage);
                    break;
                case DisconnectMessage _:
                    Console.WriteLine($"Player {peer} has disconnected.");
                    break;
            }
        }

        public IGameState Process(long dt)
        {
            return this;
        }

        private void CalculateRewards()
        {
            logger.Log("Rewards calculation not implemented yet.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver.GameStates
{
    internal enum GameStateId
    {
        Created,
        CountdownToStart,
        Started,
        Finished
    }
}

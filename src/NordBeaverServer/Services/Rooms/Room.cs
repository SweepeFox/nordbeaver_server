﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.EventLoops;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver;
using By.Gamefactory.NordBeaver.Services.Rooms.RoomStates;
using NordBeaver.Services.Rooms.NordBeaver;
using System;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    partial class Room
    {
        public static readonly long ReportRoomStateTimerMs = 1000;
        public static readonly long StartupCountdownMs = 1000;
        public static readonly long GameStateSendIntervalMs = 100;
        public readonly int RoomId;
        private readonly RoomsService roomsService;
        private readonly FramePulser framePulser;
        private readonly ILogger logger;
        private readonly NordBeaverGame game;
        private IRoomState currentState;
        private IRoomState gameFinished;
        private long roomStateReportTimerMs;
        public RoomUser User => roomUser;
        private RoomUser roomUser;

        public bool Finished { get => currentState == gameFinished; }

        public Room(int roomId, RoomsService roomsService, FramePulser framePulser, ILogger logger)
        {
            RoomId = roomId;
            this.roomsService = roomsService;
            this.framePulser = framePulser;
            this.logger = logger;
            game = new NordBeaverGame(framePulser, logger);
            gameFinished = new GameFinishedRoomState(logger, this);
            IRoomState gameStarted = new GameStartedRoomState(gameFinished, game, roomUser, this, logger);
            IRoomState startupCountdownState = new StartupCountdownRoomState(gameStarted, game, logger, this);
            IRoomState waitingPlayerState = new WaitingPlayerRoomState(startupCountdownState, this, logger);
            currentState = waitingPlayerState;
            currentState.OnEnter();
            roomStateReportTimerMs = ReportRoomStateTimerMs;
            this.framePulser.OnPulse += FramePulser_OnPulse;
        }

        private void FramePulser_OnPulse(long dt)
        {
            IRoomState nextState = currentState.Process(dt);
            if (nextState != currentState)
            {
                currentState.OnExit();
                currentState = nextState;
                currentState.OnEnter();
            }
            roomStateReportTimerMs -= dt;
            if (roomStateReportTimerMs < 0)
            {
                roomStateReportTimerMs = ReportRoomStateTimerMs;
                ReportRoomState();
            }
            if (Finished)
            {

                framePulser.OnPulse -= FramePulser_OnPulse;
            }
        }

        public void ProcessMessage(int peer, DefaultMessage message)
        {
            if (message.GetType() == typeof(PongMessage))
            {
                SendOutgoingMessage(peer, new PingMessage());
                return;
            }
            currentState.ProcessMessage(peer, message);
        }

        public void SendOutgoingMessage(int peer, DefaultMessage message)
        {
            if (roomUser == null)
            {
                return;
            }
            roomsService.SendMessasgeToMessagesService(peer, RoomId, message);
        }

        public void SendOutgoingMessage(DefaultMessage message)
        {
            if (roomUser == null)
            {
                return;
            }
            try
            {
                roomsService.SendMessasgeToMessagesService(roomUser.peer, RoomId, message);
            }
            catch (Exception ex)
            {
                logger.Log(ErrorMessageCode.FailedToSendMessageToRoom, ex);
            }
        }

        public void RequestForPeerDisconnect(int peer)
        {
            roomsService.RequestForPeerDisconnection(peer);
        }

        private void SendGameConfig(int peer)
        {
            var testRecords = new TestRecords();
            GameConfigMessage message = new GameConfigMessage
            {
                allTimeRecords = testRecords.AllTimeRecordsToBytes(),
                monthRecords = testRecords.MonthRecordsToBytes(),
                weekRecords = testRecords.WeekRecordsToBytes()
            };
            SendOutgoingMessage(peer, message);
        }

        private void ReportRoomError(int peer, ErrorMessageCode code)
        {
            SendOutgoingMessage(peer, new ErrorMessage { errorCode = (byte)code });
        }

        private void ReportRoomError(ErrorMessageCode code)
        {
            SendOutgoingMessage(new ErrorMessage { errorCode = (byte)code });
        }

        private void ReportRoomState()
        {
            SendOutgoingMessage(new RoomStateMessage
            {
                state = (byte)currentState.StateId,
                stateTimerSec = (byte)currentState.GetTimer()
            });
        }

        private void AddPlayer(int userId, int battleId, int peer)
        {
            roomUser = new RoomUser(userId, battleId, peer);
        }
    }

    class RoomUser
    {
        public readonly int userId;
        public readonly int battleId;
        public readonly int peer;
        
        public RoomUser(int userId, int battleId, int peer)
        {
            this.userId = userId;
            this.battleId = battleId;
            this.peer = peer;
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms.RoomStates
{
    internal interface IRoomState
    {
        RoomStateId StateId { get; }
        void OnEnter();
        void OnExit();
        IRoomState Process(long dt);
        void ProcessMessage(int peer, DefaultMessage message);
        long GetTimer();
    }
}

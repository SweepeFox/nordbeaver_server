﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.RoomStates;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    partial class Room
    {
        internal class GameFinishedRoomState : IRoomState
        {
            private readonly ILogger logger;
            private readonly Room room;

            public GameFinishedRoomState(ILogger logger, Room room)
            {
                this.logger = logger;
                this.room = room;
            }

            public RoomStateId StateId => RoomStateId.GameFinished;

            public long GetTimer()
            {
                return 0;
            }

            public void OnEnter()
            {
                logger.Log("[ROOM] Entered GameFinishedRoomState.");
            }

            public void OnExit()
            {
                logger.Log("[ROOM] Exited GameFinishedRoomState.");

            }

            public IRoomState Process(long dt)
            {
                return this;
            }

            public void ProcessMessage(int peer, DefaultMessage message)
            {
                switch (message)
                {
                    case JoinRoomMessage _:
                        room.ReportRoomError(peer, ErrorMessageCode.GameFinished);
                        break;
                }
            }
        }
    }
}

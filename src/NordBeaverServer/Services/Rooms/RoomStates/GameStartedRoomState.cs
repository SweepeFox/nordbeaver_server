﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver;
using By.Gamefactory.NordBeaver.Services.Rooms.RoomStates;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    partial class Room
    {
        internal class GameStartedRoomState : IRoomState
        {
            private readonly IRoomState next;
            private readonly NordBeaverGame game;
            private readonly RoomUser roomUser;
            private readonly Room room;
            private readonly ILogger logger;
            private long millisToSendGameState;
            private bool leave;

            public GameStartedRoomState(
                IRoomState next, 
                NordBeaverGame game,
                RoomUser roomUser, 
                Room room,
                ILogger logger
            )
            {
                this.next = next;
                this.game = game;
                this.roomUser = roomUser;
                this.room = room;
                this.logger = logger;
                millisToSendGameState = 0;
            }

            public RoomStateId StateId => RoomStateId.GameStarted;

            public long GetTimer()
            {
                return 0;
            }

            public void OnEnter()
            {
                logger.Log("[ROOM] Entered GameStartedRoomState.");
            }

            public void OnExit()
            {
                logger.Log("[ROOM] Exited GameStartedRoomState.");
            }

            public IRoomState Process(long dt)
            {
                if (game.Finished || leave)
                {
                    return next;
                }
                millisToSendGameState -= dt;
                if (millisToSendGameState <= 0)
                {
                    millisToSendGameState = GameStateSendIntervalMs;
                    room.SendOutgoingMessage(game.GetGameState());
                }
                return this;
            }

            public void ProcessMessage(int peer, DefaultMessage message)
            {
                switch (message)
                {
                    case JoinRoomMessage joinRoomMessage:
                        //room.SendGameConfig(peer);
                        break;
                    case UserInputMessage userInputMessage:
                        game.ProcessMessage(peer, userInputMessage);
                        break;
                    case DisconnectMessage disconnectMessage:
                        room.RequestForPeerDisconnect(peer);
                        game.ProcessMessage(peer, disconnectMessage);
                        leave = true;
                        break;
                }
            }
        }
    }
}

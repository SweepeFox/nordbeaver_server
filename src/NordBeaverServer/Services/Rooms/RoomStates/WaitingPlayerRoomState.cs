﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.RoomStates;
using System;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    partial class Room
    {
        internal class WaitingPlayerRoomState : IRoomState
        {
            private readonly IRoomState next;
            private readonly Room room;
            private readonly ILogger logger;
            private bool hasRoomUser;

            public WaitingPlayerRoomState(
                IRoomState next,
                Room room,
                ILogger logger
            )
            {
                this.next = next;
                this.room = room;
                this.logger = logger;
            }

            public RoomStateId StateId => RoomStateId.WaitingForPlayers;

            public long GetTimer()
            {
                return 0;
            }

            public void OnEnter()
            {
                logger.Log("[ROOM] Entered WaitingForPlayersRoomState.");
            }

            public void OnExit()
            {
                logger.Log("[ROOM] Exited WaitingForPlayersRoomState.");
            }

            public IRoomState Process(long dt)
            {
                if (hasRoomUser)
                {
                    return next;
                }
                return this;
            }

            public void ProcessMessage(int peer, DefaultMessage message)
            {
                switch (message)
                {
                    case JoinRoomMessage joinRoomMessage:
                        try
                        {
                            room.AddPlayer(joinRoomMessage.userId, joinRoomMessage.battleId, peer);
                            hasRoomUser = true;
                        }
                        catch (Exception ex)
                        {
                            room.ReportRoomError(peer, ErrorMessageCode.FailedToJoinRoom);
                            logger.Log(ErrorMessageCode.FailedToJoinRoom, ex);
                        }
                        break;
                }
            }
        }
    }
}

﻿namespace By.Gamefactory.NordBeaver.Services.Rooms.RoomStates
{
    enum RoomStateId
    {
        WaitingForPlayers,
        StartupCountdownStarted,
        MindplaysStartGame,
        GameStarted,
        GameFinished,
        RewardsGranted
    }
}

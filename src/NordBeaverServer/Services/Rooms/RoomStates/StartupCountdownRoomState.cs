﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using By.Gamefactory.NordBeaver.Services.Rooms.NordBeaver;
using By.Gamefactory.NordBeaver.Services.Rooms.RoomStates;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    partial class Room
    {
        internal class StartupCountdownRoomState : IRoomState
        {
            private readonly IRoomState next;
            private readonly NordBeaverGame game;
            private readonly ILogger logger;
            private readonly Room room;

            public StartupCountdownRoomState(IRoomState next, NordBeaverGame game, ILogger logger, Room room)
            {
                this.next = next;
                this.game = game;
                this.logger = logger;
                this.room = room;
            }

            public RoomStateId StateId => RoomStateId.StartupCountdownStarted;

            public long GetTimer()
            {
                return 0;
            }

            public void OnEnter()
            {
                logger.Log("[ROOM] Entered StartupCountdownRoomState.");
                game.Start(room.User);
            }

            public void OnExit()
            {
                logger.Log("[ROOM] Exited StartupCountdownRoomState.");
            }

            public IRoomState Process(long dt)
            {
                //room.SendGameConfig(room.User.peer);
                return next;
            }

            public void ProcessMessage(int peer, DefaultMessage message)
            {
                switch (message)
                {
                    case DisconnectMessage disconnectMessage:
                        room.RequestForPeerDisconnect(peer);
                        game.ProcessMessage(peer, disconnectMessage);
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    class RoomsServiceConfig
    {
        public readonly int MaxRoomsOnServer = 250;
        public readonly long FinishedRoomsCheckIntervalMs = 1000;
    }
}

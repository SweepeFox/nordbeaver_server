﻿using By.Gamefactory.NordBeaver.Logs;
using By.Gamefactory.NordBeaver.Services.EventLoops;
using By.Gamefactory.NordBeaver.Services.Messages;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using Disruptor;
using System;
using System.Collections.Generic;
using System.Threading;
using TRoomId = System.Int32;

namespace By.Gamefactory.NordBeaver.Services.Rooms
{
    class RoomsService : IEventHandler<IncomingMessage>
    {
        private readonly RoomsServiceConfig roomsServiceConfig;
        private readonly ILogger logger;
        private readonly Dictionary<TRoomId, Room> rooms;
        private ReaderWriterLockSlim roomsLock;
        private FramePulser framePulser;
        private DisruptorMessageService messagesService;
        private readonly List<int> finishedRoomsIds;
        private long currentTimerMs;

        public RoomsService(RoomsServiceConfig roomsServiceConfig, ILogger logger)
        {
            this.roomsServiceConfig = roomsServiceConfig;
            this.logger = logger;
            finishedRoomsIds = new List<int>(roomsServiceConfig.MaxRoomsOnServer / 2);
            rooms = new Dictionary<TRoomId, Room>(roomsServiceConfig.MaxRoomsOnServer);
            roomsLock = new ReaderWriterLockSlim();
        }

        public void SendMessageToRoom(TRoomId roomId, int peer, DefaultMessage message)
        {
            if (roomId.IsSystemRoom())
            {
                ProcessSystemMessage(peer, roomId, message);
                return;
            }
            roomsLock.EnterUpgradeableReadLock();
            try
            {
                if (!rooms.TryGetValue(roomId, out Room room))
                {
                    Room newRoom = new Room(roomId, this, framePulser, logger);
                    roomsLock.EnterWriteLock();
                    try
                    {
                        rooms.Add(roomId, newRoom);
                        room = newRoom;
                    }
                    catch (Exception ex)
                    {
                        ReportError(peer, roomId, ErrorMessageCode.FailedToAddNewRoom);
                        logger.Log(ErrorMessageCode.FailedToAddNewRoom, ex);
                        return;
                    }
                    finally
                    {
                        roomsLock.ExitWriteLock();
                    }
                }
                room.ProcessMessage(peer, message);
            }
            catch (Exception ex)
            {
                ReportError(peer, roomId, ErrorMessageCode.FailedToSendMessageToRoom);
                logger.Log(ErrorMessageCode.FailedToSendMessageToRoom, ex);
            }
            finally
            {
                roomsLock.ExitUpgradeableReadLock();
            }
        }

        public void SendMessasgeToMessagesService(int peer, int roomId, DefaultMessage message)
        {
            messagesService.EnqueueOutgoingMessage(peer, roomId, message);
        }

        public void RequestForPeerDisconnection(int peer)
        {
            messagesService.EnqueueForceDisconnect(peer);
        }

        public void Start(DisruptorMessageService messagesService, FramePulser framePulser)
        {
            this.framePulser = framePulser;
            this.messagesService = messagesService;
            currentTimerMs = roomsServiceConfig.FinishedRoomsCheckIntervalMs;
            this.framePulser.OnPulse += FramePulser_OnPulse;
        }

        private void FramePulser_OnPulse(long dt)
        {
            currentTimerMs -= dt;
            if (currentTimerMs <= 0)
            {
                ProcessFinishedRooms();
                currentTimerMs = roomsServiceConfig.FinishedRoomsCheckIntervalMs;
            }
        }

        private void ProcessSystemMessage(int peer, int roomId, DefaultMessage message)
        {
            switch (message)
            {
                case DisconnectMessage disconnect:
                    roomsLock.EnterReadLock();
                    try
                    {
                        foreach (Room room in rooms.Values)
                        {
                            room.ProcessMessage(peer, disconnect);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Log(ErrorMessageCode.FailToProcessDisconnectMessage, ex);
                    }
                    finally
                    {
                        roomsLock.ExitReadLock();
                    }
                    break;
            }
        }

        private void ProcessFinishedRooms()
        {
            roomsLock.EnterReadLock();
            try
            {
                foreach (var room in rooms)
                {
                    if (room.Value.Finished)
                    {
                        finishedRoomsIds.Add(room.Key);
                        room.Value.ProcessMessage(-1, new DisconnectMessage { reason = DisconnectReason.RoomFinished});
                    }
                }
            }
            catch (Exception ex) 
            {
                logger.Log(ErrorMessageCode.FailedToProcessFinishedRooms, ex);
            }
            finally { roomsLock.ExitReadLock(); }
            roomsLock.EnterWriteLock();
            try
            {
                foreach (int roomId in finishedRoomsIds)
                {
                    rooms.Remove(roomId);
                    logger.Log($"[ROOMS SERVICE] Removed room {roomId}.");
                }
                finishedRoomsIds.Clear();
            }
            catch (Exception ex)
            {
                logger.Log(ErrorMessageCode.FailedToProcessFinishedRooms, ex);
            }
            finally { roomsLock.ExitWriteLock(); }
        }

        public void Stop()
        {
            framePulser.OnPulse -= FramePulser_OnPulse;
        }

        public void OnEvent(IncomingMessage data, long sequence, bool endOfBatch)
        {
            SendMessageToRoom(data.roomId, data.fromPeer, data.message);
        }

        private void ReportError(int peer, TRoomId roomId, ErrorMessageCode errorMessageCode)
        {
            SendMessasgeToMessagesService(peer, roomId, new ErrorMessage { errorCode = (byte)errorMessageCode });
        }
    }

    enum SystemRoomID
    {
        SystemRoom = -1
    }

    static class SystemRoomExtention
    {
        public static bool IsSystemRoom(this int roomId)
        {
            return roomId == (int)SystemRoomID.SystemRoom;
        }
    }
}

﻿using System;

namespace By.Gamefactory.NordBeaver.Services.Emitters
{
    class UserInteractEmitter
    {
        public event Action<int, int, bool> OnUserKilled;

        public void EmitKill(int killerId, int victimId, bool isSuicide)
        {
            OnUserKilled?.Invoke(killerId, victimId, isSuicide);
        }
    }
}

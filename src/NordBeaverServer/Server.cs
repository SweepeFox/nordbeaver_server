﻿using By.Gamefactory.NordBeaver.Logs.Loggers;
using By.Gamefactory.NordBeaver.Network;
using By.Gamefactory.NordBeaver.Services.EventLoops;
using By.Gamefactory.NordBeaver.Services.Messages;
using By.Gamefactory.NordBeaver.Services.Rooms;

namespace By.Gamefactory.NordBeaver
{
    class Server
    {
        private WSSServer networkServer;
        private DisruptorMessageService messagesService;
        private RoomsService roomsService;
        private FramePulser framePulser;

        public Server(string pathToServerConfig)
        {
            networkServer = new WSSServer(new WSSServerConfig(pathToServerConfig));
            roomsService = new RoomsService(new RoomsServiceConfig(), new ConsoleLogger());
            messagesService = new DisruptorMessageService(new MessagesServiceConfig());
            framePulser = new FramePulser();
        }

        public void Start()
        {
            messagesService.Start(networkServer, roomsService, networkServer);
            roomsService.Start(messagesService, framePulser);
            networkServer.Start(messagesService);
            framePulser.Start();
        }

        public void Stop()
        {
            messagesService.Stop();
            roomsService.Stop();
            networkServer.Stop();
            framePulser.Stop();
        }
    }
}

﻿using By.Gamefactory.NordBeaver.Services.Messages;
using By.Gamefactory.NordBeaver.Services.Messages.Types;
using System;
using Fleck;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Threading;
using Disruptor;

namespace By.Gamefactory.NordBeaver.Network
{
    class WSSServer : IEventHandler<OutgoingMessage>, IEventHandler<ForceDisconnect>
    {
        private DisruptorMessageService messagesService;
        private readonly WebSocketServer internalServer;
        private readonly WSSServerConfig serverConfig;
        private readonly Dictionary<int, IWebSocketConnection> peers;
        private readonly ReaderWriterLockSlim peersLock;
        private Thread serverThread;
        private readonly Action DoNothing = () => { };

        public WSSServer(WSSServerConfig serverConfig)
        {
            this.serverConfig = serverConfig;
            if (this.serverConfig.Secure && string.IsNullOrEmpty(this.serverConfig.PathToCertificate))
            {
                throw new Exception("Failed to create websocket server. Config required secured connections, but no path to certificate provided.");
            }
            string proto = this.serverConfig.Secure ? "wss" : "ws";
            string url = $"{proto}://{this.serverConfig.Host}:{this.serverConfig.Port}";
            internalServer = new WebSocketServer(url);
            if (this.serverConfig.Secure)
            {
                internalServer.Certificate = new X509Certificate2(this.serverConfig.PathToCertificate);
            }
            peers = new Dictionary<int, IWebSocketConnection>(serverConfig.MaxPeersConnected);
            peersLock = new ReaderWriterLockSlim();
        }

        public void Start(DisruptorMessageService messagesService)
        {
            serverThread = new Thread(new ThreadStart(() =>
                {
                    this.messagesService = messagesService;
                    internalServer.Start(socket =>
                    {
                        
                        int peerId = socket.ConnectionInfo.Id.GetHashCode();
                        socket.OnOpen = () => OnConnected();
                        socket.OnBinary = (bytes) => OnBinaryMessageReceived(peerId, bytes);
                        socket.OnClose = () => OnDisconnected(peerId);
                        peersLock.EnterWriteLock();
                        try
                        {
                            peers.Add(peerId, socket);
                        }
                        catch
                        {
                            socket.OnClose = DoNothing;
                            socket.Close();
                        }
                        finally { peersLock.ExitWriteLock(); }
                    });
                }
            ));
            serverThread.Start();
        }

        private void OnConnected()
        {
            Console.WriteLine("Connected");
        }

        private void OnDisconnected(int peerId)
        {
            lock (peersLock)
            {
                peers.Remove(peerId);
            }
            OnMessage(peerId, -1, new DisconnectMessage { reason = DisconnectReason.SocketClosed });
        }

        private void OnBinaryMessageReceived(int peerId, byte[] messageBytes)
        {
            try
            {
                Type messageType = MessageTypeResolver.TypeCodeToType(messageBytes[DefaultMessage.MESSAGE_TYPE_POSITION]);
                int roomId = BitConverter.ToInt32(messageBytes, DefaultMessage.MESSAGE_ROOMID_POSITION);
                DefaultMessage message = (DefaultMessage)Activator.CreateInstance(messageType);
                message.FromBytes(messageBytes);
                OnMessage(peerId, roomId, message);
            } 
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void OnMessage(int peer, int roomId, DefaultMessage message)
        {
            messagesService.EnqueueIncomingMessage(peer, roomId, message);
        }

        public void Send(int peer, byte[] message)
        {
            IWebSocketConnection socket;
            peersLock.EnterReadLock();
            try
            {
                peers.TryGetValue(peer, out socket);
            }
            finally { peersLock.ExitReadLock(); }
            if (socket != null)
            {
                if (!socket.IsAvailable)
                {
                    return;
                }
                socket.Send(message);
            }
        }

        public void Stop()
        {
            internalServer.ListenerSocket.Close();
            internalServer.Dispose();
        }

        public void ForceDisconnect(int peer)
        {
            IWebSocketConnection socket;
            peersLock.EnterUpgradeableReadLock();
            try
            {
                if (peers.TryGetValue(peer, out socket))
                {
                    peersLock.EnterWriteLock();
                    try
                    {
                        peers.Remove(peer);
                    }
                    finally { peersLock.ExitWriteLock(); }
                }
            }
            finally { peersLock.ExitUpgradeableReadLock(); }
            if (socket != null)
            {
                socket.OnClose = DoNothing;
                socket.Close();
            }
        }

        public void OnEvent(OutgoingMessage data, long sequence, bool endOfBatch)
        {
            if (!data.Single)
            {
                int[] peers = data.ToPeers;
                byte[] messageBytes = data.Message.ToBytes(data.RoomId);
                for (int i = 0; i < peers.Length; i++)
                {
                    Send(peers[i], messageBytes);
                }
            }
            else
            {
                Send(data.Peer, data.Message.ToBytes(data.RoomId));
            }
        }

        public void OnEvent(ForceDisconnect data, long sequence, bool endOfBatch)
        {
            ForceDisconnect(data.peer);
        }
    }
}

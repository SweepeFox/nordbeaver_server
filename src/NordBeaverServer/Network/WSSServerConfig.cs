﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace By.Gamefactory.NordBeaver.Network
{
    class WSSServerConfig
    {
        public int Port { get; private set; } = 8555;
        public string Host { get; private set; } = "0.0.0.0";
        public bool Secure { get; private set; } = false;
        public string PathToCertificate { get; private set; } = "";
        public int MaxPeersConnected { get; private set; } = 1000;

        public WSSServerConfig(string configFilePath)
        {
            if (string.IsNullOrEmpty(configFilePath))
            {
                return;
            }
            byte[] contents = File.ReadAllBytes(configFilePath);
            ServerConfigData configData = JsonConvert.DeserializeObject<ServerConfigData>(System.Text.Encoding.Default.GetString(contents));
            AssignConfigData(configData);
        }

        private void AssignConfigData(ServerConfigData configData)
        {
            Port = configData.port;
            Host = configData.host;
            Secure = configData.secure;
            PathToCertificate = configData.path_to_certificate;
            MaxPeersConnected = configData.max_peers;
        }
    }

    internal class ServerConfigData
    {
        public int port;
        public string host;
        public bool secure;
        public string path_to_certificate;
        public int max_peers;
    }
}

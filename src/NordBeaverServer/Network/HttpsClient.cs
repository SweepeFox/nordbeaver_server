﻿using By.Gamefactory.NordBeaver.Logs;
using System;
using System.Net.Http;
using System.Text;

namespace By.Gamefactory.NordBeaver.Network
{
    class HttpsClient
    {
        private readonly HttpClient client = new HttpClient();
        protected readonly ILogger logger;

        public HttpsClient(ILogger logger)
        {
            this.logger = logger;
        }

        public HttpRequestTracker Post(string url, string data)
        {
            try
            {
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                var task = client.PostAsync(new Uri(url), content);
                return new HttpRequestTracker(task, logger);
            }
            catch (Exception e)
            {
                logger.Log(e.ToString());
                return null;
            }
        }

        public HttpRequestTracker Get(string url)
        {
            var task = client.GetAsync(new Uri(url));
            return new HttpRequestTracker(task, logger);
        }
    }
}

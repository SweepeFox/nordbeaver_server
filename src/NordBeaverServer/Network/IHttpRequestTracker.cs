﻿using System;
using System.Collections.Generic;
using System.Text;

namespace By.Gamefactory.NordBeaver.Network
{
    interface IHttpRequestTracker<T>
    {
        event Action<T> OnResponse;
        event Action<Exception, string> OnError;
    }
}

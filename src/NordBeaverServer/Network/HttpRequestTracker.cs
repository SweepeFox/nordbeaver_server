﻿using By.Gamefactory.NordBeaver.Logs;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace By.Gamefactory.NordBeaver.Network
{
    class HttpRequestTracker : IHttpRequestTracker<string>
    {
        public event Action<string> OnResponse;
        public event Action<Exception, string> OnError;

        protected readonly ILogger logger;

        public HttpRequestTracker(Task<HttpResponseMessage> request, ILogger logger)
        {
            this.logger = logger;
            var awaiter = request.GetAwaiter();
            awaiter.OnCompleted(() => OnMessage(awaiter.GetResult()));
        }

        protected virtual void OnMessage(HttpResponseMessage responseMessage)
        {
            var responseContent = responseMessage.Content.ReadAsStringAsync().Result;
            try
            {
                responseMessage.EnsureSuccessStatusCode();
                OnResponse?.Invoke(responseContent);
            }
            catch(Exception e)
            {
                logger.Log(responseContent);
                OnError?.Invoke(e, responseContent);
            }
        }
    }
}
